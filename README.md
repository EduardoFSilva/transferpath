<div align=center>
    ![Logo TP](./src/main/resources/res/icon/256x.png)
<br><br>
</div>

<center><h4 align="center" style="text-align: center;">Transferpath: Software de transferência de arquivos por HTTP</h4></center>

<hr/>

<div align="center">

![Bootstrap](https://img.shields.io/badge/Bootstrap%205.0-%23563D7C.svg?style=for-the-badge&logo=bootstrap&logoColor=white)
![Java](https://img.shields.io/badge/Java%208-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![HTML](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![CSS](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)
![jQuery](https://img.shields.io/badge/jquery-%230769AD.svg?style=for-the-badge&logo=jquery&logoColor=white)
</div>

# **Requisitos mínimos**

*   Java 8 update 144 ou superior
*   Apache Maven 3.0.5 ou superior (apenas para compilação manual)

# **Compilando**
## Via linha de comando

#### 1.   Clone o repositório usando o comando abaixo em uma pasta vazia.
```bash
git clone https://gitlab.com/EduardoFSilva/Transferpath.git .
```
#### 2.   No diretório criado. Execute o Apache Maven
```bash
mvn clean assembly:assembly
```
Caso Ocorra. O erro <b>Error reading assemblies: No assembly descriptors found</b> Poderá ser ignorado

#### 3.  Copie o arquivo ./target/Transferpath-{versao}-jar-with-dependencies.jar para o local desejado.

## Através de uma IDE
#### 1.  Clone o repositório usando o comando abaixo em uma pasta vazia.
```bash
git clone https://gitlab.com/EduardoFSilva/Transferpath.git .
```
#### 2.  Abra o diretório na IDE de sua preferência como um projeto Java Maven

#### 3.  Inicie o processo de build. 

#### 4.  Copie o arquivo ./target/Transferpath-{version}-jar-with-dependencies.jar para o local desejado

# **Executando a aplicação**
1.1. Execute o comando abaixo caso esteja em um sistema Unix Based
```bash
 java -jar Transferpath-{versao}-jar-with-dependencies.jar
```
1.2. Dê dois cliques na no executavel Java `**Transferpath-{versao}-jar-with-dependencies.jar**`

2. Aguarde o programa criar estrutura básica de pastas abaixo:

```
./Data
    ╠ /files
    ╚ config.bin
```
3. Aguarde até que a aplicação altere o Status para **Executando** e acesse **`http//localhost:{porta}`** ou **`http//{ipRedeLocal}:{porta}`** para acessar em outra máquina

Os arquivos podem ser enviados na parte superior, e baixado, apagados ou abertos na parte inferior.

### FAQ:
P: Porque a aplicação não abre?<br>
R: Atualmente existe um bug nas versões mais recentes do java onde a tela poderá não abrir. Uma correção já está sendo providenciada

P: Porque a aplicação só mantém o Status em **Erro**?<br>
R: A porta selecionada pode estar em uso.

P: Como descobrir o IP da minha máquina na rede local?<br>
R: Utilize **ifconfig** em máquinas Unix e **ipconfig** em máquinas Windows

P: Porque não consigo acessar a aplicação em máquinas remotas?<br>
R: O Firewall pode estar bloqueando. Verifique as políticas de rede.
<br><br>
# **Autor**
* Eduardo F. Silva
