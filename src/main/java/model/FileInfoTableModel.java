/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDateTime;
import java.util.List;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import util.Converter;

/**
 *
 * @author Usuario
 */
public class FileInfoTableModel extends AbstractTableModel {

    private List<FileInfo> list;
    private static Converter converter;
    private static final String[] columnNames = {"Arquivo", "Modificado Em"};
    private static final Class[] columnClasses = {String.class, LocalDateTime.class};
    private static final boolean[] canEdit = {false, false};

    static {
        converter = new Converter();
    }

    public FileInfoTableModel(List<FileInfo> list) {
        this.list = list;
    }

    public FileInfoTableModel() {
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return canEdit.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        FileInfo obj = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return obj.getFilename();
            case 1:
                return converter.convertDate(obj.getLastModified());
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClasses[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit[columnIndex];
    }

    public List<FileInfo> getList() {
        return list;
    }

    public void setList(List<FileInfo> list) {
        this.list = list;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        FileInfo info = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                info.setFilename((String) value);
                break;
            case 1:
                info.setLastModified(converter.convertToLocalDateTime(value));
                break;
        }
        list.set(rowIndex, info);
    }
    

}
