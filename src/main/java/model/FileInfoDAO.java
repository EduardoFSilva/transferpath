/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import util.Constants;
import util.Converter;

/**
 *
 * @author Usuario
 */
public class FileInfoDAO {

    private static Converter converter;

    static {
        converter = new Converter();
    }

    public static List<FileInfo> fillFileInfoList() {
        FileInfo infoFile;
        List<FileInfo> infoList = new ArrayList<>();
        File filesFolder = new File(Constants.FILES_FOLDER);
        File[] fileArray = filesFolder.listFiles();
        if (fileArray != null) {
            for (File file : fileArray) {
                if (file.isFile()) {
                    infoFile = new FileInfo();
                    infoFile.setFilename(file.getName());
                    infoFile.setLastModified(converter.convertToLocalDateTime(file.lastModified()));
                    infoList.add(infoFile);
                }
            }
        }
        return infoList;
    }
}
