package transferpath.routes;

import com.google.common.net.UrlEscapers;
import java.awt.Desktop;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import model.FileInfo;
import model.FileInfoDAO;
import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.io.EofException;
import spark.*;
import spark.template.velocity.VelocityTemplateEngine;
import transferpath.config.ConfigObject;
import transferpath.config.ConfigObjectManager;
import util.Constants;
import util.Converter;

public class TransferPathRoutes {
    //Velocity Template Engine Variable Creation

    private static VelocityTemplateEngine templateEngine;
    //Spark Model And View Variable Creation
    private static ModelAndView modelAndView;
    //Reserving Variable For Context Data
    private static Map<String, Object> context;
    //General Converter
    private static Converter converter;
    //File List
    private static List<FileInfo> info;
    //Map used to send allowed options to the template engine
    private static Map<String, Boolean> allow;

    //Static Constructor
    static {
        templateEngine = new VelocityTemplateEngine("UTF-8");
        context = new HashMap<>();
        converter = new Converter();
    }

    /**
     * Index Page Function
     *
     * @param request
     * @param response
     * @return
     */
    public static String doGetIndexPage(Request request, Response response) {
        //Context Map Filling
        context.clear();
        context.put("version", Constants.VERSION);
        //Template Engine
        modelAndView = new ModelAndView(context, "/velocity/index-vm.html");
        return templateEngine.render(modelAndView);
    }

    /**
     * Home Page Function
     *
     * @param request
     * @param response
     * @return
     */
    public static String doGetTransferPathPage(Request request, Response response) {
        //Context And Session Startup
        context.clear();
        Session session = request.session();
        //Checks if have messages on the current session
        if (session.attribute("msg") != null) {
            context.put("msg", ((HashMap<String, String>) session.attribute("msg")));
            session.attribute("msg", null);
        }
        //Checks if have files on the Data/files folder
        info = FileInfoDAO.fillFileInfoList();
        if (!info.isEmpty()) {
            context.put("fileinfolist", info);
        }
        //Puts Objects on context
        context.put("converter", converter);
        getAllowMap();
        context.put("allow", allow);
        context.put("interval", ConfigObjectManager.getObjectManager().readConfigFromMemory().getUpdateInterval());
        context.put("async", ConfigObjectManager.getObjectManager().readConfigFromMemory().isAsyncUpdate());
        context.put("version", Constants.VERSION);
        //Template Engine
        modelAndView = new ModelAndView(context, "/velocity/transferpath-vm.html");
        return templateEngine.render(modelAndView);
    }

    /**
     * File Receiver Function
     *
     * @param request
     * @param response
     * @return
     */
    public static String doPostReceive(Request request, Response response) {
        //String return if is forbidden
        String retStr = "";
        //Jetty Configuration Informations Are: Dest. Folder, File Size Max, Request Size Max and Threshold
        request.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement(Constants.TMP_FOLDER, Long.MAX_VALUE, Long.MAX_VALUE, 0x2000000));
        try {
            //Gets File Part
            Part filePart = request.raw().getPart("arquivo");
            //Gets Session
            Session session = request.session();
            //Creates A Message Map
            Map<String, String> msg = new HashMap<>();
            //Verify If The Software is configured to receive files
            if (ConfigObjectManager.getObjectManager().readConfigFromMemory().isAllowedSending()) {
                //Verifies if the request isn't empty
                if (filePart.getSize() > 0) {
                    //Saves File
                    boolean ret = saveFile(filePart);
                    //Build Message
                    msg.put("type", ret ? "success" : "error");
                    msg.put("text", String.format(ret ? "O arquivo <b>%s</b> foi recebido com sucesso!"
                            : "Um erro ocorreu ao receber o arquivo <b>%s</b>", filePart.getSubmittedFileName()));
                } else {
                    //Runs If File Size equals zero, what means that no files was received
                    msg.put("type", "error");
                    msg.put("text", "Erro: Nenhum Arquivo Foi Recebido");
                }
                //Puts message on the session object
                session.attribute("msg", msg);
                //Redirects back to the home page
                response.redirect("/transferpath");
            } else {
                //Return a Forbidden if sending isn't enable
                response.status(403); //Forbidden
                String bodyText = "<span class='text-danger'>Acesso Negado</span>. Você não tem permissão para enviar arquivos";
                retStr = buildStatusPage(request, response, 403, "Proíbido", bodyText);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //Deletes the Multipart receiving folder
        Constants.deleteTemporaryFolder();
        return retStr;

    }

    /**
     * Function used to save files
     *
     * @param part
     * @return <b>true</b> if the files was successfully saved, and <b>false</b>
     * if not
     */
    private static boolean saveFile(Part part) {
        try {
            //Part Input Stream
            InputStream is = part.getInputStream();
            //Output File and Files Ouput Stream
            File outputFile = new File(String.format(Constants.getFileDestination(part.getSubmittedFileName())));
            FileOutputStream fos = new FileOutputStream(outputFile);
            //Buffers Up Input/Output to prevent heap overflow
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            BufferedInputStream bis = new BufferedInputStream(is);
            //Copies files
            IOUtils.copy(bis, bos);
            //Cleases Stream
            bis.close();
            bos.close();
            fos.close();
            //Success
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            //Fail
            return false;
        }
    }

    /**
     * File Provider Function
     *
     * @param request
     * @param response
     * @return
     */
    public static String doGetDownloadFile(Request request, Response response) {
        //Reads if is allowed downloading
        if (ConfigObjectManager.getObjectManager().readConfigFromMemory().isAllowedDownloading()) {
            //Info page message, only used on fails
            Map<String, String> mess = new HashMap<>();
            //Reads if file name was received
            if (request.params("filename") != null) {
                //Creates File object to Be Sended and verifies if exists
                File retrieveFile = new File(Constants.getFileDestination(request.params("filename")));
                if (retrieveFile.exists()) {
                    //String that holds mime type
                    String type;
                    try {
                        //Gets MIME type
                        type = Files.probeContentType(retrieveFile.toPath());
                        //Create The Headers
                        response.header("Content-Type", String.format("%s;charset=UTF-8", type));
                        response.header("Content-Disposition", String.format("attachment; filename=%s", UrlEscapers.urlFormParameterEscaper().escape(retrieveFile.getName())));
                        response.header("Content-Length", Long.toString(retrieveFile.length())); //File Size
                        //Buffers Jetty Output Stream with 16 MB
                        BufferedOutputStream bos = new BufferedOutputStream(response.raw().getOutputStream(), 0x1000000);
                        //Opens InputStream for Local File 
                        FileInputStream fis = new FileInputStream(retrieveFile);
                        //Buffers In Local File on 16 MB
                        BufferedInputStream bis = new BufferedInputStream(fis, 0x1000000);
                        byte[] bytes = new byte[0x200000]; // 2MB
                        int len;
                        while ((len = bis.read(bytes, 0, bytes.length)) > 0) {
                            bos.write(bytes);
                        }
                        bos.flush();
                        bos.close();
                        bis.close();
                        fis.close();
                        return "";
                    } catch (EofException ex) {
                        
                    } catch (Exception ex) {
                        //Put this messages if some error like a IOException happens
                        ex.printStackTrace();
                        mess.put("title", "Um Erro Ocorreu");
                        mess.put("body", String.format("Ocorreu um erro ao tentar baixar o arquivo"));
                    }

                } else {
                    //Put this message if the file was not found
                    mess.put("title", "Um Erro Ocorreu");
                    mess.put("body", String.format("O arquivo solicitado não foi encontrado!"));
                }
            } else {
                //Put this nessage if the file name was not received
                mess.put("title", "Informação");
                mess.put("body", String.format("O nome do arquivo precisa ser fornecido para que seja baixado."));
            }
            //Returns the Page if is allowed
            context.clear();
            mess.put("pgtitle", "Download");
            context.put("version", Constants.VERSION);
            context.put("mess", mess);
            modelAndView = new ModelAndView(context, "/velocity/message-vm.html");
            return templateEngine.render(modelAndView);
        } else {
            //Returns forbidden if isn't
            response.status(403); //Forbidden
            String bodyText = "<span class='text-danger'>Acesso Negado</span>. Você não tem permissão para baixar arquivos";
            return buildStatusPage(request, response, 403, "Proíbido", bodyText);
        }
    }

    /**
     * Delete File Function
     *
     * @param request
     * @param response
     * @return
     */
    public static String doGetDeleteFile(Request request, Response response) {
        //Vefify if deleting is enabled
        if (ConfigObjectManager.getObjectManager().readConfigFromMemory().isAllowedDeleting()) {
            //Create Message Map
            Map<String, String> mess = new HashMap<>();
            //Checks if file name got received
            if (request.params("filename") != null) {
                File fileToRemove = new File(Constants.getFileDestination(request.params("filename")));
                //Checks if the file exists
                if (fileToRemove.exists()) {
                    try {
                        //Removes The File
                        fileToRemove.delete();
                        mess.put("title", "Arquivo Removido Com Sucesso");
                        mess.put("body", "Pressione o botão abaixo para voltar a página inicial");
                    } catch (Exception ex) {
                        //Triggers if removing the file failed
                        ex.printStackTrace();
                        mess.put("title", "Erro");
                        mess.put("body", "Algo impediu que o arquivo fosse removido. Favor verificar permissões de escrita");
                    }
                } else {
                    //Triggers if file not exists
                    mess.put("title", "Erro");
                    mess.put("body", "O arquivo solicitado para remoção não existe");
                }
            } else {
                //Triggers if the file name not got provided
                mess.put("title", "Informação");
                mess.put("body", String.format("O nome do arquivo precisa ser fornecido para que seja removido."));
            }
            //Normal Return
            context.clear();
            context.put("version", Constants.VERSION);
            context.put("mess", mess);
            modelAndView = new ModelAndView(context, "/velocity/delete-vm.html");
            return templateEngine.render(modelAndView);
        } else {
            //Forbidden Return
            response.status(403); //Forbidden
            String bodyText = "<span class='text-danger'>Acesso Negado</span>. Você não tem permissão para apagar arquivos";
            return buildStatusPage(request, response, 403, "Proíbido", bodyText);
        }
    }

    /**
     * Asynchronous Page HTML Generator
     *
     * @param request
     * @param response
     * @return
     */
    public static String doGetAjaxTable(Request request, Response response) {
        context.clear();
        //Gets Files For List Displaying
        info = FileInfoDAO.fillFileInfoList();
        if (!info.isEmpty()) {
            context.put("fileinfolist", info);
        }
        //Renders Front End
        context.put("converter", converter);
        getAllowMap();
        context.put("allow", allow);
        context.put("interval", ConfigObjectManager.getObjectManager().readConfigFromMemory().getUpdateInterval());
        modelAndView = new ModelAndView(context, "/velocity/ajaxbody-vm.html");
        return templateEngine.render(modelAndView);
    }

    /**
     * Opens The Selected file on the host computer
     *
     * @param request
     * @param response
     * @return
     */
    public static String doGetOpenFile(Request request, Response response) {
        //Checks if is allowed to open files into the host computer
        if (ConfigObjectManager.getObjectManager().readConfigFromMemory().isAllowedOpening()) {
            //Creates Message Map
            Map<String, String> mess = new HashMap<>();
            //Checks if filename isn't null
            if (request.params("filename") != null) {
                //Creates The File object and check if exists
                File fileToOpen = new File(Constants.getFileDestination(request.params("filename")));
                if (fileToOpen.exists()) {
                    //Checks if the desktop is supported
                    if (Desktop.isDesktopSupported()) {
                        try {
                            //Opens the File
                            Desktop.getDesktop().open(fileToOpen);
                            mess.put("title", "Abrindo Arquivo");
                            mess.put("body", String.format("O arquivo <b>%s</b> foi aberto. Você já pode fechar esta janela.", fileToOpen.getName()));
                        } catch (IOException ex) {
                            //Triggers if file fails to open
                            ex.printStackTrace();
                            mess.put("title", "Erro Ao Abrir Arquivo");
                            mess.put("body", String.format("Um erro de I/O ocorreu ao tentar abrir o arquivo"));
                        }
                    } else {
                        //Triggers if desktop isn't supported
                        mess.put("title", "Erro Ao Abrir Arquivo");
                        mess.put("body", String.format("A abertura de arquivos não é suportada por este sistema"));
                    }
                } else {
                    //Triggers if File do not exists
                    mess.put("title", "Erro Ao Abrir Arquivo");
                    mess.put("body", "O arquivo solicitado para abertura não existe");
                }
            } else {
                mess.put("title", "Informação");
                mess.put("body", String.format("O nome do arquivo precisa ser fornecido para que seja visualizado."));
            }
            //Returns if not goes wrong
            mess.put("pgtitle", "Abrir Arquivo");
            context.put("version", Constants.VERSION);
            context.put("mess", mess);
            modelAndView = new ModelAndView(context, "/velocity/message-vm.html");
            return templateEngine.render(modelAndView);
        } else {
            //Returns if opening files is disabled
            response.status(403); //Forbidden
            String bodyText = "<span class='text-danger'>Acesso Negado</span>. Você não tem permissão para abrir arquivos";
            return buildStatusPage(request, response, 403, "Proíbido", bodyText);
        }
    }

    /**
     * Creates Pages For The Program
     *
     * @param request Request Object from Caller Route
     * @param response Response Object from Caller Route
     * @param code HTTP Status Code
     * @param title Page Title
     * @param text Text Message Displayed Below Status Number
     * @return The Rendered Page HTML
     */
    public static String buildStatusPage(Request request, Response response, int code, String title, String text) {
        //Status map to be processed by Apache Velocity
        Map<String, String> status = new HashMap<>();
        status.put("title", title); //Page Title
        status.put("code", String.format("%d", code)); //Status Code
        status.put("text", text); //Info Text
        response.status(code); //Configuring Status
        context.clear();//Cleaning Context
        context.put("version", Constants.VERSION);
        context.put("status", status);
        modelAndView = new ModelAndView(context, "/velocity/status-vm.html");
        return templateEngine.render(modelAndView);
    }

    /**
     * Returns Not Found Page Into a Route Format for Spark
     *
     * @param request
     * @param response
     * @return
     */
    public static String doGetNotFoundPage(Request request, Response response) {
        String origem = String.format("A página <span class='text-primary'>%s</span> não podê ser encontrada", request.pathInfo());
        response.status(404);
        return buildStatusPage(request, response, 404, "Página Não Encontrada", origem);
    }

    //Creates A Allow Map Flag for Operations Send, delete, open and download, checjing if supported
    private static void getAllowMap() {
        allow = new HashMap<>();
        ConfigObject co = ConfigObjectManager.getObjectManager().readConfigFromMemory();
        allow.put("send", co.isAllowedSending());
        allow.put("delete", co.isAllowedDeleting());
        allow.put("open", co.isAllowedOpening());
        allow.put("download", co.isAllowedDownloading());

    }
}
