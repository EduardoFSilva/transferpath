package transferpath.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import util.Constants;

public class ConfigObjectManager {

    private static ConfigObject configObject = null;
    private static ConfigObjectManager objectManager = null;

    private ConfigObjectManager() {

    }

    public static ConfigObjectManager getObjectManager() {
        if (objectManager == null) {
            objectManager = new ConfigObjectManager();
        }
        return objectManager;
    }

    public boolean writeConfigToDisk(ConfigObject configObject) {
        boolean success = false;
        File configFile = new File(getConfigFilePath());
        try {
            if (!(configFile.exists() && configFile.isFile())) {
                if (!(configFile.getParentFile().exists() && configFile.getParentFile().isDirectory())) {
                    configFile.getParentFile().mkdirs();
                }
            }
            if(configObject == null){
                configObject = new ConfigObject().loadDefault();
            }
            FileOutputStream fos = new FileOutputStream(configFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(configObject);
            oos.close();
            fos.close();
            ConfigObjectManager.configObject = configObject;
            success = true;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return success;
    }

    public ConfigObject readConfigFromMemory() {
        if (configObject == null) {
            configObject = readConfigFromDisk();
        }
        return configObject;
    }

    public ConfigObject readConfigFromDisk() {
        ConfigObject co = null;
        File configFile = new File(getConfigFilePath());
        try {
            if (!(configFile.exists() && configFile.isFile())) {
                writeConfigToDisk(co);
            }
            FileInputStream fis = new FileInputStream(configFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            co = (ConfigObject) ois.readObject();
            ois.close();
            fis.close();
            configObject = co;
        } catch (ClassNotFoundException | IOException ex) {
            ex.printStackTrace();
        }
        return co;
    }

    private static String getConfigFilePath() {
        return String.format("%s%c%s", Constants.DATA_FOLDER_PATH, File.separatorChar, Constants.CONFIG_FILE_NAME);
    }
    
    public static boolean configFileExists(){
        File f = new File(getConfigFilePath());
        return f.exists();
    }

}
