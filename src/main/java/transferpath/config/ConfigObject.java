package transferpath.config;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public class ConfigObject implements Serializable {

    private boolean allowedSending;
    private boolean allowedDownloading;
    private boolean allowedOpening;
    private boolean allowedDeleting;
    private boolean useHttps;
    private boolean logFileOperations;
    private boolean asyncUpdate;
    private boolean autoStart;
    private int port;
    private int updateInterval;
    
    public ConfigObject loadDefault() {
        this.allowedDeleting = true;
        this.allowedDownloading = true;
        this.allowedOpening = true;
        this.allowedSending = true;
        this.useHttps = false;
        this.logFileOperations = false;
        this.autoStart = true;
        this.asyncUpdate = false;
        this.port = 4567;
        this.updateInterval = 10000;
        return this;
    }

    public boolean isAsyncUpdate() {
        return false;
        //return asyncUpdate;
    }

    public void setAsyncUpdate(boolean asyncUpdate) {
        this.asyncUpdate = asyncUpdate;
    }

    
    public int getUpdateInterval() {
        return updateInterval;
    }

    public void setUpdateInterval(int updateInterval) {
        this.updateInterval = updateInterval;
    }

    public boolean isAutoStart() {
        return autoStart;
    }

    public void setAutoStart(boolean autoStart) {
        this.autoStart = autoStart;
    }

    
    public boolean isAllowedSending() {
        return allowedSending;
    }

    public void setAllowedSending(boolean allowedSending) {
        this.allowedSending = allowedSending;
    }

    public boolean isAllowedDownloading() {
        return allowedDownloading;
    }

    public void setAllowedDownloading(boolean allowedDownloading) {
        this.allowedDownloading = allowedDownloading;
    }

    public boolean isAllowedOpening() {
        return allowedOpening;
    }

    public void setAllowedOpening(boolean allowedOpening) {
        this.allowedOpening = allowedOpening;
    }

    public boolean isAllowedDeleting() {
        return allowedDeleting;
    }

    public void setAllowedDeleting(boolean allowedDeleting) {
        this.allowedDeleting = allowedDeleting;
    }

    public boolean isUseHttps() {
        return useHttps;
    }

    public void setUseHttps(boolean useHttps) {
        this.useHttps = useHttps;
    }

    public boolean isLogFileOperations() {
        return logFileOperations;
    }

    public void setLogFileOperations(boolean logFileOperations) {
        this.logFileOperations = logFileOperations;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    

}
