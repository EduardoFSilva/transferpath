/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transferpath;

/**
 *
 * @author Usuario
 */
import static spark.Spark.*;
import static spark.debug.DebugScreen.enableDebugScreen;
import transferpath.routes.TransferPathRoutes;
import util.Constants;
import util.Globais;

public class HTTPServerStart {

    private static boolean running = false;
    private static boolean failed = false;

    public static void startServer(String ip, int port) {
        initRouteVariables();
        running = true;
        failed = false;
        staticFiles.location("/res");
        //staticFiles.externalLocation(Constants.DATA_FOLDER_PATH);
        ipAddress(ip);
        port(port);
        if (Globais.useHTTPS) {
            secure(Constants.KEYSTORE_PATH, "tsfre44dev", null, null);
        }
        initExceptionHandler((e) -> handleException(e));
        setupRoutes();
        enableDebugScreen();
        init();
        awaitInitialization();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
        }
        if (failed) {
            running = false;
        }
    }

    public static void stopServer() {
        stop();
        awaitStop();
        running = false;
    }

    public static boolean isRunning() {
        return running;
    }

    public static boolean failed() {
        return failed;
    }

    private static void handleException(Exception ex) {
        failed = true;
        running = false;
        ex.printStackTrace();
    }

    private static void initRouteVariables() {

    }

    private static void setupRoutes() {
        notFound(TransferPathRoutes::doGetNotFoundPage);
        post("/receive", "*/*", TransferPathRoutes::doPostReceive);
        get("/transferpath", TransferPathRoutes::doGetTransferPathPage);
        get("/", TransferPathRoutes::doGetIndexPage);
        get("/ajaxtable", TransferPathRoutes::doGetAjaxTable);
        path("/download", () -> {
            get("/:filename", TransferPathRoutes::doGetDownloadFile);
            get("/", TransferPathRoutes::doGetDownloadFile);
            get("", TransferPathRoutes::doGetDownloadFile);
        });
        path("/delete", () -> {
            get("/:filename", TransferPathRoutes::doGetDeleteFile);
            get("/", TransferPathRoutes::doGetDeleteFile);
            get("", TransferPathRoutes::doGetDeleteFile);
        });
        path("/open", () -> {
            get("/:filename", TransferPathRoutes::doGetOpenFile);
            get("/", TransferPathRoutes::doGetOpenFile);
            get("", TransferPathRoutes::doGetOpenFile);
        });
    }

}
