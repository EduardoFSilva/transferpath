package util;

import java.io.File;

public class Constants {

    private static File tmpFile;

    public static final String VERSION = "1.1.3";

    //Name From Data Folder
    public static final String DATA_FOLDER_NAME = "Data";
    //Name From Config File
    public static final String CONFIG_FILE_NAME = "config.bin";
    //Name From File Folder
    public static final String FILE_FOLDER_NAME = "files";
    //Keystore name
    public static final String KEYSTORE_NAME = "keystore.jks";

    //Dynamic Path From Data Folder - Changes If the Jar Location got changed too
    public static final String DATA_FOLDER_PATH = String.format("%s%c%s", new File("").getAbsolutePath(), File.separatorChar, DATA_FOLDER_NAME);
    //Dynamic Path From Data Folder - Changes If the Jar Location got changed too
    public static final String CONFIG_FILE_PATH = String.format("%s%c%s", DATA_FOLDER_PATH, File.separatorChar, DATA_FOLDER_NAME);
    //
    public static final String FILES_FOLDER = String.format("%s%c%s", DATA_FOLDER_PATH, File.separatorChar, FILE_FOLDER_NAME);
    //Keystore path
    public static final String KEYSTORE_PATH = String.format("%s%c%s", DATA_FOLDER_PATH, File.separatorChar, KEYSTORE_NAME);
    //Temporary Folder
    public static final String TMP_FOLDER = String.format("%s%c%s", FILES_FOLDER, File.separatorChar, "temp");

    public static String getFileDestination(String filename) {
        return String.format("%s%c%s", FILES_FOLDER, File.separatorChar, filename);
    }

    public static void deleteTemporaryFolder() {
        for(File f : tmpFile.listFiles()){
            f.delete();
        }
        tmpFile.delete();
    }

    static {
        tmpFile = new File(TMP_FOLDER);
    }
}
