package util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Converter {
    
    private static SimpleDateFormat sdf;
    private static Date dateHolder;
    static{
        sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    }
    public String convertDate(LocalDateTime date){
        return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
    }
    public String convertDate(Date date){
        return sdf.format(date);
    }
    public LocalDateTime convertToLocalDateTime(Object date){
        if(date instanceof String){
            return LocalDateTime.parse((String)date,DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        }
        return (LocalDateTime) date;
    }
    
    public String convertDate(long stamp){
        dateHolder = new Date(stamp);
        return sdf.format(dateHolder);
    }
    public LocalDateTime convertToLocalDateTime(Date date){
        String formatted = convertDate(date);
        return LocalDateTime.parse(formatted, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
    }
    public LocalDateTime convertToLocalDateTime(long date){
        String formatted = convertDate(date);
        return LocalDateTime.parse(formatted, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
    }
}
