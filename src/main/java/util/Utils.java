/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import transferpath.config.*;

/**
 *
 * @author Usuario
 */
public class Utils {
    
    public static void createIfNotExists(){
        File f;
        f = new File(Constants.DATA_FOLDER_PATH);
        if(!(f.exists() && f.isDirectory())){
            f.mkdir();
        }
        f = new File(Constants.FILES_FOLDER);
        if(!(f.exists() && f.isDirectory())){
            f.mkdir();
        }
        if(!ConfigObjectManager.configFileExists()){
            ConfigObjectManager.getObjectManager().writeConfigToDisk(new ConfigObject().loadDefault());
        }
    }
}
