package main;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.util.Locale;
import java.util.Scanner;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import org.slf4j.LoggerFactory;
import transferpath.HTTPServerStart;
import transferpath.config.ConfigObjectManager;
import util.Utils;
import view.ControlPanelVIEW;

public class Main {

    private static boolean quickstart = false;
    private static String ip = "0.0.0.0";
    private static int port = 4567;
    private static Thread consoleHttpThread;
    private static Scanner sc;

    public static void main(String[] args) {
        //Disable Logback
        ((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).setLevel(Level.OFF);
        //Force Language
        Locale.setDefault(Locale.forLanguageTag("pt-BR"));
        //Tratamento de Argumentos
        for (String arg : args) {
            if (arg.toLowerCase().startsWith("--quickstart") || arg.equals("-q")) {
                quickstart = true;
            }
            if (arg.toLowerCase().startsWith("--ip:") || arg.startsWith("-i:")) {
                String[] sp = arg.split(":");
                if (sp.length > 1) {
                    if (!sp[1].trim().equals("")) {
                        ip = sp[1];
                    }
                }
            }
            if (arg.toLowerCase().startsWith("--port:") || arg.startsWith("-p:")) {
                String[] sp = arg.split(":");
                if (sp.length > 1) {
                    if (!sp[1].trim().equals("")) {
                        try {
                            int number = Integer.parseInt(sp[1]);
                            port = (number > 0 && number < 65536) ? number : port;
                        } catch (NumberFormatException ex) {

                        }
                    }
                }
            }
        }
        //Initializer
        init();
    }

    public static void init() {
        Utils.createIfNotExists();
        ConfigObjectManager.getObjectManager().readConfigFromMemory();
        sc = new Scanner(System.in);
        if (quickstart) {
            createConsoleHttpThread();
            consoleHttpThread.start();
            try {
                Thread.sleep(1000);
                sc.nextLine();
                System.out.println("Parando Servidor...");
                HTTPServerStart.stopServer();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else{
            try {
                UIManager.setLookAndFeel(new NimbusLookAndFeel());
            } catch (UnsupportedLookAndFeelException ex) {
                ex.printStackTrace();
            }
            ControlPanelVIEW cpv = new ControlPanelVIEW();
            cpv.setVisible(true);
        }
    }

    private static void createConsoleHttpThread() {
        consoleHttpThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HTTPServerStart.startServer(ip, port);
                if (!HTTPServerStart.failed()) {
                    System.out.println(String.format("Transfer Path Iniciado Em: http://%s:%d", ip, port));
                    System.out.print(String.format("\nPressiona Enter Para Parar o HTTP "));
                } else {
                    System.out.println("Falha Ao Iniciar o HTTP Do Transfer Path");
                }
                while (HTTPServerStart.isRunning()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
                System.out.println("Servidor Interrompido Com Sucesso");
                consoleHttpThread.interrupt();
                consoleHttpThread = null;
                if (HTTPServerStart.failed()) {
                    try {
                        Thread.sleep(150);
                    } catch (InterruptedException ex) {
                    }
                    HTTPServerStart.stopServer();
                    System.out.println("Servidor Interrompido");
                }
            }
        });
    }

}
