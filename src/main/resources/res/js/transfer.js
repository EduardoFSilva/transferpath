function setupDatatable() {
    $('#tbfiles').DataTable({
        "language": {
            "url": "/vendor/datatables/Portuguese-Brasil.json"
        },
        "order": [[0, "asc"]],
        pageLength: 5,
        "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]]
    });
}

var shouldUpdate = true;

function setShouldUpdate(val) {
    shouldUpdate = val;
}

function getShouldUpdate() {
    return shouldUpdate;
}

function checkForFiles() {
    var vidFileLength = $("#videoUploadFile")[0].files.length;
    setShouldUpdate(vidFileLength === 0);
}

function remover(filename) {
    let modalTarefa = document.querySelector('#fileNameSpan');
    let removeId = document.querySelector('#fileNameInput');
    modalTarefa.innerHTML = filename;
    removeId.value = filename;
    mostrarModal();
}

function removerArquivo() {
    let removeId = document.querySelector('#fileNameInput');
    window.location.replace("/delete/" + removeId.value);
    esconderModal();
}

function mostrarModal() {
    $('#modalRemove').modal('show');
}

function esconderModal() {
    $('#modalRemove').modal('hide');
}